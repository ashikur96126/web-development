<?php 
    define('TITLE','Null Data & Resource');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
</head>

<body>
    
   <h1><?php echo TITLE ?></h1>
    
    <?php
        $nullVariabel=null;
        var_dump($nullVariabel);
        /*****************************
        if we want to handle a file than we have to call a handle called fopen();
        fopen(@param1,@param3)
        ----------
        (required)
        ----------
        @param1 = fileName,
        @param2 = open, read, and close,
        
        *******************************/
        
        $text = fopen('file/text.txt','r');
        
        
        /*****************************
        if we want to handle a file than we have to call a handle called fopen();
        fopen(@param1,@param3)
        ----------
        (required)
        ----------
        @param1 = resource,
        @param2 = length,
        
        *******************************/
        
        $myFile = fread($text,filesize('file/text.txt'));
        
        echo '<br>';
        
        echo $myFile;
        
        /*****************************
        if we want to handle a file than we have to call a handle called fopen();
        fopen(@param1,@param3)
        ----------
        (required)
        ----------
        @param1 = word will be replaced,
        @param2 = word to be replaced,
        @param3 = string resource,
        ----------
        (optional)
        ----------
        @param4 = how many words are replaced
        
        *******************************/
        $sentence = "I am a Boy";
        
        $replace_sentence =  str_replace('Boy','Girl',$sentence);
        
        echo '<hr><br>'.$replace_sentence;
        
        
        echo '<hr><br>'.$sentence;
        
        
        $translate = str_replace('file','ফাইল',$myFile,$xxxxxxxxxx);
        
        echo '<br>'.$xxxxxxxxxx;
        echo '<br>'.$translate;


        
    ?> 
    
</body>

</html>
