<?php 
    define('TITLE','Object in PHP');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
</head>

<body>
    
   <h1><?php echo TITLE ?></h1>
 <?php
    class Car {
    
            //properties
            public $brand = 'BMW';
            public $color = 'Black';
            private $status = 'OFF';

    
            //Method
            public function trun_on() { 
                $this->status = 'ON';
            }

            public function show_status(){
               return $this->status; 
            }
          
        }

        $Car = new Car();
        
        echo $Car->brand;
        
        $Car->trun_on();
        
       echo '<br>'.$Car->show_status();
        
    ?> 
    
</body>

</html>
