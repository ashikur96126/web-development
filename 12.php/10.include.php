<?php define("TITLE","Include in PHP");?>
<?php 
include "file/layout/header.php";
include "file/layout/header_bottom.php";?>

<!-- Our Code will be go here -->
<?php
        date_default_timezone_set("Asia/Dhaka");
        echo date_default_timezone_get()."<br>";
        $today = date("d F Y");
        echo 'today is '.$today;

        echo '<h3><strong><i>Time using date() function</i></strong></h3>';
        $time = date('h:m:s A');
        echo 'Current time is now '.$time;

        echo '<h3><strong><i>Current timestamp using time() function:</i></strong></h3>';
        echo 'timestamp is now '.time();

        echo '<h3><strong><i>Convert timestamp to time:</i></strong></h3>';
        $timestamp = time();
        $time = date('h:m:s a',$timestamp);
        echo $time;

        echo '<h3><strong><i>Convert time to timestamp using mktime() function:</i></strong></h3>';
        $timestamp = mktime('10','32','56','6','17','2040');
        echo "10:32:56 am in 17th june 2040 to timestamp = ".$timestamp;

        //Homework
        // fixed Time to current time
?>

<?php include "file/layout/footer.php";?>
