<?php 
    define('TITLE','Array in PHP');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
</head>

<body>
    
   <h1><?php echo TITLE ?></h1>
   <?php 
        //index array
        $user = array(
            'Ashik',  //index number 0
            'Nahid',  //index number 1
            'Khokon', //index number 2
            'Based'   //index number 3
        );
        echo "<pre>";
        var_dump($user);

        echo ($user[2].'<br>');

        // Associative Array

        $userTwo = array(
            'name'=>'Ashik',        //key name
            'age'=>18,              //key age
            'profesion'=>'Student', //key profesion
            'aim'=>'developer'      //key aim
        );
        $userTwo['height']='5f';

        var_dump($userTwo);

        echo '<br>'.$userTwo['name'];
        echo '<br>'.$userTwo['age'];
        echo '<br>'.$userTwo['profesion'];
        echo '<br>'.$userTwo['aim'];
        echo '<br>'.$userTwo['height'];
        
        //multi-dimentional array
        
        $students = [
            [
                'name'=>'Ashik',        
                'age'=>18,              
                'profesion'=>'Student', 
                'aim'=>'developer' ,
                'fav'=> [
                    'Burger',
                    'Ice-cream'
                ]
            ],
            [
                'name'=>'Nahid',       
                'age'=>20,              
                'profesion'=>'Student', 
                'aim'=>'Full-Stacker'      
            ]
        ];
    ?>
    <br>
    <?php
        print_r($students);
        
        echo '<br>'.$students[0]['name'].'<br>'.$students[1]['name'];
    
        echo '<br>Favorite Food: '.$students[0]['fav'][0];
    ?>
    <br><a href="03.object.php">Go to 03.Object.php</a>
    
</body>

</html>
