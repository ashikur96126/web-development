<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login From</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="file/assets/css/bootstrap.min.css">
    <style></style>
</head>
<body>
    <div class="container">
        <div class="login-box">
        <div class="row">
            <div class="col-md-6 login-left">
                <h2>Login Here</h2>
                <form method="post" action="login.php" >
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="Email" class="form-control" placeholder="Enter your email" <?php
                               if(isset($_COOKIE["email"])){
                                   echo "value='".$_COOKIE['email']."'"; 
                               }; 
                               ?> required>
                    </div>
                    <div class="form-group">
                        <label>password</label>
                        <input type="password" name="password" class="form-control" <?php
                               if(isset($_COOKIE["pass"])){
                                   echo "value='".$_COOKIE['pass']."'"; 
                               }; 
                               ?> required>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1" name="rememberme">
                        <label class="form-check-label" for="exampleCheck1">Remember me</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                    <div><a href="forgot_password.php">Forgot your password</a></div>
                </form>
            </div>
            
            <div class="col-md-6 login-right">
                <h2>Registration Here</h2>
                <form method="post" action="<?php echo "register.php";?>" >
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="Firstname" class="form-control" placeholder="Enter Firstname" required>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="lastname" class="form-control" placeholder="Enter lastname" required>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" placeholder="Enter Email" required>
                    </div>
                    <div class="form-group">
                        <label>password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter password" required>
                    </div>
                    <div class="form-group">
                        <label>Confrim password</label>
                        <input type="password" name="Confrim_password" class="form-control" placeholder="Enter confirm password" required>
                    </div>
                    <button type="submit" name="submit_reg" class="btn btn-primary">Registration</button>
                </form>
            </div>
            
        </div>
            </div>
    </div>
<footer>
    <p class="text-center">Copyriht &copy; 2020 to <?= date("Y"); ?></p>
    </footer>
<script src="file/assets/js/jquery-3.5.1.js"></script>
<script src="file/assets/js/bootstrap.bundle.min.js"></script>
<script src="file/assets/js/sweetalert.min.js"></script>
		<?php if(!empty($_SESSION['error'])){
			?>
			<script>
				let error = document.createElement("DIV");
				error.innerHTML = '<?php echo $_SESSION['error'];?>';
				swal({
					content:error,
					button:false,
					timer:3000,
					icon:"warning",
					className:"alert",
					className:"alert-warning",
				});
			</script>
			<?php
			session_destroy();
		};
		?>
		<?php if(!empty($_SESSION['activeMessage'])){
			?>
			<script>
				let activeMessage = document.createElement("DIV");
				activeMessage.innerHTML = "<?php echo $_SESSION["activeMessage"];?>";
				swal({
					content:activeMessage,
					// button:false,
					// timer:3000,
					icon:"success",
					className:"alert",
					className:"text-success",
				});
			</script>
			<?php
			session_destroy();
		};
		?>
    </body>
</html>
