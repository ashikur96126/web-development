<?php
session_start();
if ($_SERVER["REQUEST_METHOD"]=="POST") {
    $email = $_POST["Email"];
    $mysql = new mysqli;
    $mysql->connect('localhost','root','','learningphp');
    if ($mysql->connect_error) {
        ?>
        <script>
            window.alert('Connect to Database Error ' +'<?php echo($mysql->connect_errno); ?>');
        </script>
    <?php
    };
    $result = $mysql->query("SELECT * FROM `signup` WHERE `email`='$email'");
    if($result->num_rows>0){
        $user = $result->fetch_assoc();
        $to = $user["email"];
        $subject = "Update your password";
        $token = $user["token"];
        $message = "http://localhost/LearnPhp/New%20project/update_password.php?token=$token";
        $headers= "From:ashikur96126@gmail.com";
        if(mail($to,$subject,$message,$headers)){
            $_SESSION['sendMail']="<p>Please check your mail to update your password</p>";
        }else{
            $_SESSION['error']="<p>Mail send failed try again later</p>";
        };
    }else{
        $_SESSION['error']="<p>User not found</p>";
    };
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot password</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="file/assets/css/bootstrap.min.css">
    <style></style>
</head>
<body>
    <div class="container">
        <div class="login-box">
        <div class="row">
            <div class="col login-left">
                <h2 class="text-center">Enter your Email</h2>
                <form method="post" action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" >
                    <div class="form-group">
                        <input type="text" name="Email" class="form-control" placeholder="Enter your email" <?php
                               if(isset($_COOKIE["email"])){
                                   echo "value='".$_COOKIE['email']."'"; 
                               }; 
                               ?> required>
                    </div>
                    <button type="submit" class="btn btn-primary">Send mail</button>
                </form>
            </div>  
        </div>
        </div>
    </div>
<footer>
    <p class="text-center">Copyriht &copy; 2020 to <?= date("Y"); ?></p>
    </footer>
<script src="file/assets/js/jquery-3.5.1.js"></script>
<script src="file/assets/js/bootstrap.bundle.min.js"></script>
<script src="file/assets/js/sweetalert.min.js"></script>
<?php if(!empty($_SESSION['error'])){
        ?>
        <script>
            let error = document.createElement("DIV");
            error.innerHTML = '<?php echo $_SESSION['error'];?>';
            swal({
                content:error,
                button:false,
                timer:3000,
                icon:"warning",
                className:"alert",
                className:"alert-warning",
            });
        </script>
        <?php
        session_destroy();
    };
    ?>
    <?php if(!empty($_SESSION['sendMail'])){
        ?>
        <script>
            let sendMail = document.createElement("DIV");
            sendMail.innerHTML = '<?php echo $_SESSION['sendMail'];?>';
            swal({
                content:sendMail,
                // button:false,
                // timer:3000,
                icon:"success",
                className:"alert",
                className:"text-success",
            });
        </script>
        <?php
        session_destroy();
    };
    ?>
    <?php if(!empty($_SESSION['error12'])){
        ?>
        <script>
            let error12 = document.createElement("DIV");
            error12.innerHTML = '<?php echo $_SESSION['error12'];?>';
            swal({
                content:error12,
                button:false,
                timer:3000,
                icon:"warning",
                className:"alert",
                className:"alert-warning",
            });
        </script>
        <?php
        session_destroy();
    };
    ?>
    
</body>
</html>
