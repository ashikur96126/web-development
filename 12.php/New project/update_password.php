<?php
session_start();
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST["token"])){  
    $token = $_POST["token"];  
    $password=$_POST['password'];
    $confirm_password=$_POST['confirm_password'];
    if($password==$confirm_password){
        $mysql = new mysqli;
        $mysql->connect('localhost','root','','learningphp');
        if($mysql->connect_error){
            ?>
            <script>
                window.alert('Connect to Database Error ' +'<?php echo($mysql->connect_errno); ?>');
            </script>
        <?php
        }
        $result = $mysql->query("SELECT * FROM `signup` WHERE `token`='$token'");
        if($result->num_rows>0){
            $password = password_hash($password,PASSWORD_BCRYPT);
            $newToken= bin2hex(random_bytes(15));
            $mysql->query("UPDATE `signup` SET `status`='active',`password`='$password',`token`='$newToken' WHERE token='$token'");
            if($mysql->affected_rows>0){
            $_SESSION["activeMessage"]="<p>Your password has been changed</p>";
                header("location:index.php");
            };
        }else{
            $_SESSION["error12"]="<p>Sometiong is wrong,Please re submit your gmail</p>";
            header("location:forgot_password.php");
        };
}else{
    $_SESSION["error"]="<p>password does not match</p>";
    header("location:update_password.php?token=$token");
};
};           
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Udate password</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="file/assets/css/bootstrap.min.css">
    <style></style>
</head>
<body>
    <div class="container">
        <div class="login-box">
        <div class="row">
            <div class="col login-left">
                <h2 class="text-center">Create new Password</h2>
                <form method="post" action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" >
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="New password"required>
                    </div>
                    <div class="form-group">
                        <input type="confirm_password" name="confirm_password" class="form-control" placeholder="Confirm password"required>
                        <input type="hidden" name="token" <?php if(isset($_GET["token"])){echo "value='".$_GET["token"]."'";} ?>>
                    </div>
                    <button type="submit"  class="btn btn-primary">Create password</button>
                </form>
            </div>  
        </div>
        </div>
    </div>
<footer>
    <p class="text-center">Copyriht &copy; 2020 to <?= date("Y"); ?></p>
    </footer>
<script src="file/assets/js/jquery-3.5.1.js"></script>
<script src="file/assets/js/bootstrap.bundle.min.js"></script>
<script src="file/assets/js/sweetalert.min.js"></script>
<?php if(!empty($_SESSION['error'])){
    ?>
        <script>
            let error = document.createElement("DIV");
            error.innerHTML = '<?php echo $_SESSION['error'];?>';
            swal({
                content:error,
                button:false,
                timer:3000,
                icon:"warning",
                className:"alert",
                className:"alert-warning",
            });
        </script>
        <?php
        session_destroy();
    };
    ?>
    <?php if(!empty($_SESSION['sendMail'])){
        ?>
        <script>
            let sendMail = document.createElement("DIV");
            sendMail.innerHTML = '<?php echo $_SESSION['sendMail'];?>';
            swal({
                content:sendMail,
                // button:false,
                // timer:3000,
                icon:"success",
                className:"alert",
                className:"text-success",
            });
        </script>
        <?php
        session_destroy();
    };
    ?>
</body>
</html>
