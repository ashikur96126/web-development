<?php
session_start();

if($_SERVER['REQUEST_METHOD'] = "POST"){
    if(isset($_POST['submit_reg'])){
        $firstname=$_POST['Firstname'];
        $lastname=$_POST['lastname'];
        $email=$_POST['email'];
		$password=$_POST['password'];
		$confirm_password=$_POST['Confrim_password'];
		$_SESSION['error'] = NULL;
		$token = bin2hex(random_bytes(15));
        
        if (empty($firstname)) {
			$_SESSION['error'] .= "<p>Firstname field is required</p>";
		} else {
			$first = filter_var($firstname, FILTER_SANITIZE_STRING);
		};
        
        if (empty($lastname)) {
			$_SESSION['error'] .= "<p>lastname field is required</p>";
		} else {
			$last = filter_var($lastname, FILTER_SANITIZE_STRING);
		};  
        
        if(empty($email)){
            $_SESSION['error'] .= "<p>Email field is required</p>";
        }else{
            $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            if(!filter_var($email,FILTER_SANITIZE_EMAIL)){
                $_SESSION['error'] .= "<p>Email is not valid</p>";
            }
        }; 
        if(empty($password)){
            $_SESSION['error'] .= "<p>Password field is required</p>";
        }else{
            $hash_password= password_hash($password,PASSWORD_BCRYPT);
        };
        if($password!=$confirm_password){
			$_SESSION['error'] .= "<p>Password doesn\'t match</p>";
		};
        
        if(empty($_SESSION['error'])){
            $mysql = new mysqli;
            $mysql->connect('localhost','root','','learningphp');
            if($mysql->connect_error){
                ?>
				<script>
					window.alert('Connect to Database Error ' +'<?php echo($mysql->connect_errno); ?>');
				</script>
			<?php
            }
            
            $checkEmail = $mysql->query("SELECT email FROM signup WHERE email='$email'");
            if($checkEmail->num_rows>0){
                $_SESSION['error'] .= "<p>Email is allready exist</p>";
                header("location:index.php");
            }else{
                $to = $email;
                $subject = "Active your account";
                $message = "http://localhost/LearnPhp/New%20project/active.php?token=$token";
                $headers= "From:ashikur96126@gmail.com";

                if(mail($to,$subject,$message,$headers)){
                    $query = "INSERT INTO signup(firstname,lastname,email,password,token) VALUES('$firstname','$lastname','$email','$hash_password','$token')";
                    $mysql->query($query);
                    $_SESSION['activeMessage']="<p>Please check your mail to active your account</p>";
                    header("location:index.php");
                }else{
                    $_SESSION['error']="<p>Mail send faield Please try again later!</p>";
                    header("location:index.php");
                };
                // $_SESSION['user'] = $mysql->insert_id;
                // header("Location:home.php");
                if($mysql->error){
					?>
					<script>
						swal({
							title:"Opps!",
							text:"Registration Failed",
							icon:"error",
							button:false,
							timer:3000,
							className:"alert",
							className:"alert-danger",
						});
					</script>
                <?php
                header("Location:index.php");
				}
            }
        }else{
            header("Location:index.php");
        }
    }
}  
?>
