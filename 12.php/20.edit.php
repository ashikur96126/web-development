<?php define("TITLE","Edit Profile");?>
<?php 

include "file/layout/header.php";?>
.avatar{
                vertical-align: middle;
                width: 200px;
                height: 200px;
                border-radius: 50%; 
            }
<?php
include "file/layout/header_bottom.php";
include "98.conection.php";
$userid = $_GET['id'];
$user_result = $mysql->query("SELECT * FROM register WHERE id='$userid'");
$user = $user_result->fetch_assoc();


?>

<?php

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $error = null;
        if($_POST['firstname']==null){
            $error .= "<P>Please fill up the first name field!</P>";
            $reasultMessage = "<div class='alert alert-warning'>".$error."
            </div>";
        }else{
            $firstname = filter_var($_POST['firstname'], FILTER_SANITIZE_STRING);
        }
        if($_POST['lastname']==null){
            $error .= "<P>Please fill up the last name field!</P>";
            $reasultMessage = "<div class='alert alert-warning'>".$error."
            </div>";
        }else{
            $lastname = filter_var($_POST['lastname'], FILTER_SANITIZE_STRING);
        }
        if($_POST['email']==null){
            $error .= "<P>Please fill up the Email field!</P>";
            $reasultMessage = "<div class='alert alert-warning'>".$error."
            </div>";
        }else{
            $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $error .= "<P>Please Enter a valid email address</P>";
                $reasultMessage = "<div class='alert alert-warning'>".$error."
                </div>";
            }
        }
        if($_POST['password']==null){
            $error .= "<P>Please enter your Password!</P>";
            $reasultMessage = "<div class='alert alert-warning'>".$error."
            </div>";
        }else{
            $password = $_POST['password'];
            // $password = md5($password);
            $password = password_hash($password, PASSWORD_DEFAULT);

        }
        if($_FILES['uploadimg']['error']>0){
            
            $error .= "<P>Please upload a valid Image</P>";
            $reasultMessage = "<div class='alert alert-warning'>".$error."
            </div>";
        }else{
            $file=$_FILES['uploadimg'];
            $name = $file['name'];
            $tmp_name = $file['tmp_name'];
            $uploadFile = 'file/assets/upload/';
            $destination = $uploadFile.$name;

            $imagesize = $file['size'];
            $imgExtnsn = strtolower(pathinfo($destination,PATHINFO_EXTENSION));


            $checkExtnsn = ['jpg','png','jpeg'];

            if($imagesize > 10*1024*1024){
                $error .= "<P>Your Image size is too big</P>";
                $reasultMessage = "<div class='alert alert-warning'>".$error."
                </div>";
            }elseif(!in_array($imgExtnsn,$checkExtnsn)){
                $error .= "<P>Your Image not in jpg,png or jpeg format</P>";
                $reasultMessage = "<div class='alert alert-warning'>".$error."
                </div>";
            }elseif(file_exists($destination)){
                $error .= "<P>Image that you trying to upload that allready exists</P>";
                $reasultMessage = "<div class='alert alert-warning'>".$error."
                </div>";
            }else{
                if(move_uploaded_file($tmp_name,$destination)){
                    $reasultMessage = "<div class='alert alert-success'>
                    <p>Image uplodeded successfuly</p>
                </div>";
                }
                $reasultMessage = "<div class='alert alert-warning'>
                    <p>Sorry! Upload faield something is wrong</p>
                </div>";
            };
           
        }
        
        if($error==null){
            $query = "INSERT INTO register(firstname,lastname,email,password,profile_picture) VALUES('$firstname','$lastname','$email','$password','$destination')";
            $mysql->query($query);
            if(!$mysql->error){
                $reasultMessage = "<div class='alert alert-success'>
                    <p>Form submitted successfuly</p>
                </div>";
            }
            
        }
    }

?>






<!-- Our Code will be go here -->
<h2 class="text-center">Update Profile</h2>
<img src='<?php echo $user['profile_picture'] ?>' alt="Avatar" class="avatar">
<?php
    if(isset($reasultMessage)){
        echo $reasultMessage;
    }
?>
<form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" class="form" enctype="multipart/form-data">
    <div class="form-row">
        <div class="form-group col-6">
            <label>First Name:</label>
            <input type="text" placeholder="First Name" name="firstname" value='<?php echo $user['firstname'] ?>' class="form-control">
        </div>
        <div class="form-group col-6">
            <label>Last Name:</label>
            <input type="text" placeholder="Last Name" name="lastname" value='<?php echo $user['lastname'] ?>' class="form-control">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-6">
        <label>Email:</label>
        <input type="email" placeholder="Your Email" name="email" value='<?php echo $user['email'] ?>' class="form-control">
        </div>
        <div class="form-group col-6">
            <label>Password:</label>
            <input type="password" name="password" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="uploadimg">Choose profile photo:</label>
        <input type="file" name="uploadimg" id="uploadimg" class="form-control">
    </div>

    <button type="submit" class="btn btn-outline-success mx-3 float-right">Sumbit</button>
    <button type="reset" class="btn btn-outline-success mx-3 float-right">Cancel</button>
</form>




<?php include "file/layout/footer.php";?>
