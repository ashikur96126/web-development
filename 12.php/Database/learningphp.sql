-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2020 at 03:53 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `learningphp`
--

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE `signup` (
  `id` int(255) UNSIGNED NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'inactive',
  `creat_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`id`, `firstname`, `lastname`, `email`, `password`, `token`, `status`, `creat_at`) VALUES
(1, 'ashkiyr', 'jibon', 'rakibulislam01673@yahoo.com', '$2y$10$pucXjY9aCTx9pps9XQ3PfebAmmW1v2v1xK/uUt7RdO/2XNf04vqYy', 'cec0963d99c2b7c74a94f6b57dab7c', 'active', '2020-12-11 10:53:13'),
(2, 'abu based', 'jibon', 'abubased@outlook.com', '$2y$10$HkyzYyTXRiUMKhrtbyk9y.Hpe2jZiHCdNvb.l4qEdqEWiVXW5qyU6', '2b40bbae67939969c1465b4944d57e', 'active', '2020-12-11 11:28:06'),
(3, 'Nahid', 'Islam', 'developernahidislam@gmail.com', '$2y$10$wqMIKsMvKRvRd4Zqr54R3ejROv3QiUDbBTNAieUNI6srMRgZqkPna', '881cfc7ac3e64ecd50d77aa47e3fef', 'active', '2020-12-09 09:16:48'),
(4, 'Nahid', 'islam', 'almamunshak8@gmail.com', '$2y$10$YNf3W3QkKeshHq2/zZOzcOcyYKn/kleSA0u3J65EuuqNcuyEam8se', 'daf0e72a8f9401da0accb991dde5e5', 'active', '2020-12-11 10:52:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `signup`
--
ALTER TABLE `signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `signup`
--
ALTER TABLE `signup`
  MODIFY `id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
