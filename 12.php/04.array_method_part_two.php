<?php 
    define('TITLE','Array Method Part Two');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
</head>

<body>

    <h1><?php echo TITLE ?></h1>
    <?php

    $fruits = array('mango','apple','banana','jack-fruits','Carambola');
    echo "<pre>";
    print_r($fruits);
    echo '<H1>After Array Splice</H1>';
    array_splice($fruits,0,3,array('Cucumber,Orange'));
    print_r($fruits);

    $faraz = ['fazar','jahar','asar','magrib','asa'];
    print_r($faraz);
    $sunnat = ['fazar','jahar','magrib','asa'];
    print_r($sunnat);
    $salat = array_merge($faraz,$sunnat);
    print_r($salat);
        
    print_r(array_count_values($salat));
        
        $a = [
            'I'=>"One",
            'II'=>"Two",
            'III'=>"three"
        ];
        $b = [
            'I'=>"ssf",
            'II'=>"sfgag",
            'III'=>"gasg",
            'IV'=>"Four",
            'V'=>'Five'
        ];
        
        
    print_r(array_merge($a,$b));
    print_r(array_merge($b,$a));
        
        
    if(!in_array('beter',$salat)){
        array_push($salat,'beter');
        print_r($salat);
    }
        
    ?>
    <br>


</body>

</html>
