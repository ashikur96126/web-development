<?php 
    define('TITLE','Array Method in PHP');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
    <style>
        table,td,tr{
            border: 1px solid;
            border-collapse: collapse;
        }
        table{
            width:100%;
            text-align: center;
        }
    
    </style>
</head>

<body>
    
   <h1><?php echo TITLE ?></h1>
   <?php 
        //index array
        $user = array(
            'Ashik',  //index number 0
            'Nahid',  //index number 1
            'Khokon', //index number 2
            'Based'   //index number 3
        );
        
//        array length bar korar jonnoo count($array_name);
        
        echo count($user)."<br>";
        //sort an array sort(),rsort(),ksort(),krsort();
        
        sort($user);
        
        //array to string
        $userToString = implode(",",$user);
        echo $userToString."<br>";
        
        //String to array
        
        print_r(explode(",",$userToString));
        
        for($i=0; $i<count($user); $i++){
            echo "<P>$user[$i]</P>";
        }


        // Associative Array

        $userTwo = array(
            'name'=>'Ashik',        //key name
            'age'=>18,              //key age
            'profesion'=>'Student', //key profesion
            'aim'=>'developer'      //key aim
        );
        
        
        
        
        
        foreach($userTwo as $key => $value ){
            echo "<p>$key = $value </p>";
        }
        
        //multi-dimentional array
        
        $students = [
            [
                'name'=>'Ashik',        
                'age'=>18,              
                'profesion'=>'Student', 
                'aim'=>'developer' ,
                'fav'=> [
                    'Burger',
                    'Ice-cream'
                ]
            ],
            
            [
                'name'=>'Nahid',       
                'age'=>20,              
                'profesion'=>'Student', 
                'aim'=>'Full-Stacker'      
            ]
        ];
        
        echo  "<h1>Students Table</h1><hr><table><th>Roll</th><th>Name</th><th>Age</th><th>Profesion</th><th>Aim</th>";
        
        // foreach loop for multidimentional array
        foreach($students as $index => $value){
            echo 
            "<tr>
                <td>".($index+1)."</td>";?> <?php
            foreach($value as $key=>$td){
//                echo "<td>$key</td>";
                if(!is_array($td)){
                    echo "<td>$td</td>";
                }
            }
            echo "</tr>";
        }
        echo "</table>";
        
    ?>
    <br>

    
</body>

</html>
