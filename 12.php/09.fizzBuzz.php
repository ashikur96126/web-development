<?php 
    define('TITLE','Loop in PHP');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
</head>

<body>
    
   <h1><?php echo TITLE ?></h1><hr>

    <?php

    for ($i=1; $i <= 100; $i++) { 
        switch ($i) {
            case  $i%3==0 && $i%5==0:
                echo "FizzBuzz<br>";
                break;

            case $i%3==0:
                echo "Fizz<br>";
                break;
            
            case $i%5==0:
                echo "Buzz<br>";
                break;
            
            default:
                echo $i."<br>";
                break;
        }
    }




    ?>
    
</body>

</html>
