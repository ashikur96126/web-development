<?php 
    define('TITLE','Loop in PHP');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
</head>

<body>
    
   <h1><?php echo TITLE ?></h1>
    <?php
        class Car{
            // property
            public $color = "Red";
            public $brand = "Toyota";
            Private $status = "off";

            public $userTwo = array(
                'name'=>'Ashik',
                'age'=>18,
                'profesion'=>'Student',
                'aim'=>'developer'
            );

            public function change_status()
            {
                $this->status = "on";
            }
            public function getStatus()
            {
                return $this->status;
            }
        }

        $myObj = new Car;

        $userLength = count($myObj->userTwo);
        //For Loop
        ?>
        <h1>For loop</h1><hr><?php
        for($i=0; $i<=$userLength; $i++ ){
            echo $i."<br>";
        }

        echo "<strong>&dollar;i</strong> I will be <span style='color:red;'>".($userLength+1)."</span> because of variable <strong>&dollar;i</strong> is allready increment after echo last time<br>";

        ?>
        <h1>While loop</h1><hr><?php
        while ($i <= 10) {
        $i++;
        echo $i."<br>";
        }
        echo "<strong>&dollar;i</strong> I will be not changed  <span style='color:red;'>".$i."</span> because of variable <strong>&dollar;i</strong> is  increment before echo last time<br>";

        ?>
        <h1>Do While loop</h1><hr><?php

        do {
            echo $i."<br>";
            $i--;
        } while ($i >= 5);
        
        ?>
        <h1>Switch Statement in PHP</h1><hr><?php
        switch ($i) {
            case '1':
                echo "&dollar;i = One"; 
                break;
            case '2':
                echo "&dollar;i = Two"; 
                break;
            case '3':
                echo "&dollar;i = Three"; 
                break;
            case '4':
                echo "&dollar;i = Four";
                break;
            case '5':
                echo "&dollar;i = Five";
                break;
            default:
            echo "&dollar;i = ".$i;
                break;
        }


        // $i এর  মান ১ থেকে ১০০ পর্যন্ত  বের করতে হবে।  তারপর প্রতিবার $i এর মানকে ট্রান্সলেট করবে ইংরেজিতে 


    ?>
    
</body>

</html>
