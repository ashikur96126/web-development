<?php 
    //How to define a constant
    define('TITLE','PHP BASIC');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?php echo TITLE ?>
    </title>
</head>

<body>
    
    
    <?php 
        // how to define a variable in php
        $variableName = "Hello";
        
        //    differance between double quotation and single quotation
        echo '<hr>'. $variableName . ' Nahid<hr>';
        echo "<hr> {$variableName} Nahid<hr>";
        echo '<hr> {$variableName} Nahid<hr>';
        
        echo 5+5; 
    ?>
    
<!--   how to write php in a script tag-->
    <script>
        var variableName = "Hello";
        document.write("<hr>" + variableName + " Ashik<hr>");
        
        <?php echo "document.write(5+5+'<hr><h1>How to concate in PHP</h1><hr>');";?>
        
    </script>
    
    

    
</body>

</html>
