<?php
session_start();
define("TITLE","Cookie and Session in PHP");?>
<?php 
include "file/layout/header.php";
include "file/layout/header_bottom.php";?>

<!-- Our Code will be go here -->
<h1>Cookie</h1><hr>

<?php
    // set a cookie
    setcookie('user','Ashik',time()+86400,'/');
    setcookie('email','ashik@example.com',time()+86400,'/');

    // Delete cookie
    setcookie('email','ashik@example.com',time()-86400,'/');

    if (count($_COOKIE)>0) {        
        echo "<pre>";
        print_r($_COOKIE);
    }else{
        echo "<p>Sorry! There is no cookie set</p>";
    }

?>

<h1>Session</h1><hr>
<?php

    $_SESSION["user"] = "nahid";
    $_SESSION["login_status"] = true;

    if(isset( $_SESSION["user"]) && isset($_SESSION["login_status"])){
        echo "<p>Sessin set successfuly</p>";
    }

    if(count($_SESSION)>0){
        echo "<pre>";
        print_r($_SESSION);
    }

    // unset session
    session_unset();
    // delete session
    session_destroy();
?>

<?php include "file/layout/footer.php"; ?>
