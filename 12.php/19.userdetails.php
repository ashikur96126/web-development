<?php
    include "98.conection.php";
    $query = "SELECT * FROM register";
    $results = $mysql->query($query);
    
?>
<!DOCTYPE html>
<html lang="mul">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
        <title>Login form</title>
        <link rel="stylesheet" href="file/assets/css/bootstrap.min.css">
        <style>
            .avatar{
                vertical-align: middle;
                width: 200px;
                height: 200px;
                border-radius: 50%; 
            }
        </style>
   </head>
   <body>
       <div class="container">
       <div class="row">
        <div class="col mt-3">
           </div>
            <div class="col mt-3">
           <h2 class="text-center">User Details</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($results->num_rows>0){
                            while($row = $results->fetch_assoc()){
                               ?>
                                <tr>
                                    <td><?php echo $row['firstname']?></td>
                                    <td><?php echo $row['lastname']?></td>
                                    <td><?php echo $row['email']?></td>
                                    <td><a href="<?php echo '20.edit.php?id='.$row['id'];?>">Edit</a></td>
                                    <td><a href="<?php echo 'delete.php?id='.$row['id'];?>">Delete</a></td>
                                </tr>
                            <?php
                            }
                        }
                    ?>
                    
                </tbody>
                </table>
            </div>
       </div>
       </div>
       
       
       
       
       
        <script src="file/assets/js/jquery-3.5.1.js"></script>
        <script src="file/assets/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
