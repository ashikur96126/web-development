<?php 
    define('TITLE','Object in PHP');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo TITLE ?></title>
</head>

<body>
    
   <h1><?php echo TITLE ?></h1>
 <?php
    class Car{
        // property
        public $color = "Red";
        public $brand = "Toyota";
        Private $status = "off";

        public function change_status()
        {
            $this->status = "on";
        }
        public function getStatus()
        {
            return $this->status;
        }
    }

    $myObj = new Car;

    echo $myObj->color;
    echo "<br>";
    echo $myObj->brand;
    $myObj->change_status();
    echo "<br>".$myObj->getStatus();
?> 
    
</body>

</html>
