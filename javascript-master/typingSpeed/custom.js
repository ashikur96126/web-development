var userName;
var startTime;
var endTime;
var result;
var userString = document.getElementById("userString");
var message = document.getElementById("message");
var btn = document.getElementById("btn");
const keywords = [
    "Design is not just what it looks like and how it feels. Design is how it works.",
    "You can't just ask customers what they want and then try to give that to them. By the time you get it built, they'll want something new.",
    "A quick brown fox jump over the lazy dog",
    "The javascript this keyword refer to the object it belongs to",
    "This keyword has differente values depending on where its used",
    "Alone, This refer to the Global Object",
    "In a regular function this refer to the Global Object",
    "In a method this refer to the owner Object",
    "Execution contex the environment which our code is executed and is evaluted",
    "Coding teaches us to find out that everything has a creator."

]
const btnStyle = {
    "box-shadow" : "0 0px 40px #45ff72",
    "color" : "white",
    "background" : "#083e4e6b",
}
document.getElementById("btn1").onclick=function () {
    let name = document.getElementById("userName").value;
    if(name.length != 0){
        userName = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
        hide("getUserName");
        document.getElementById("user").innerText = userName;
        show("greeting");
        show("userString");
        show("btn");
        userString.disabled = true;
    }
}
btn.onclick=function () {
    if(this.innerText=="Start"){
        this.innerText = "Done";
        // apply styles to the button
        Object.assign(this.style, btnStyle);
        var randomNumber = Math.floor(Math.random()*keywords.length);
        do{
            message.innerText = keywords[randomNumber];
        }while (randomNumber == Math.floor(Math.random()*keywords.length));
        show("message");
        userString.disabled = false;
        userString.value = "";
        userString.autofocus = true;
        let d = new Date;
        startTime = d.getTime();
    }else{
        this.innerText = "Start";
        let d = new Date;
        endTime = d.getTime();
        let totalTime = (endTime-startTime)/1000;
        let totalWord = userString.value.split(" ").length;
        result = Math.round((totalWord/totalTime)*60);
        message.innerText = "You can type "+ result +" word per minute. and "+compareWords(userString.value,message.innerText);
    }
}
function compareWords (str1,str2) {
    let arrayOne = str1.split(" ");
    let arrayTwo = str2.split(" ");
    let correctWord = 0;
    arrayTwo.forEach((element,index) => {
        if (element == arrayOne[index]) {
            correctWord++;
        }        
    });
    let wrongWord = (arrayTwo.length - correctWord);
    return ( " you are type "+ correctWord + " correct word & " + wrongWord + " wrong word.");    
}

function show(id) {
    document.getElementById(id).style.display="block";
}
function hide(id) {
    document.getElementById(id).style.display="none";
}



