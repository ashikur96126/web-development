/***************
*  Logic one   *
****************/

var playing = false;
var score;
var timeremening;
var myInterval;
var correctAnswer;
var correctPosition;

//if we click on the start/reset button
document.getElementById("startreset").addEventListener("click",startgame);

function startgame(){
    if(!playing){
        playing = true;
        //if we are not playing
        //set score to zero
        score = 0;
        document.getElementById("scorevalue").innerHTML=score;
        //set countdown box display to block
        show("timeremaining");
        //set gameover message div display to none
        hide("gameOver");
        //change Start Game to Reset Game
        document.getElementById("startreset").innerHTML="Reset Game";
        //reduce time by 1 sec loops
        countdown();
        qustionAndAnswer(); 
    }else{
    //Reload the page
        location.reload();
    }
}

function hide(id){
    document.getElementById(id).style.display="none";
}

function show(id){
     document.getElementById(id).style.display="block";
}

function countdown (){
    timeremening = 60;
    myInterval = setInterval(function(){
        timeremening -=1 ;
    document.getElementById("timeremainingvalue").innerHTML=timeremening;
        if(timeremening==0){
            playing = false;
            clearInterval(myInterval);
            show("gameOver");
            hide("timeremaining");
            document.getElementById("startreset").innerHTML="Start Game";
            document.getElementById("gameOver").innerHTML="<p>Game is Over!</P><p>Your score is"+score+"</p>";
        }
    },1000);
}


function qustionAndAnswer(){
    var x = 1+Math.floor(Math.random()*10);
    var y = 1+Math.floor(Math.random()*10);
    document.getElementById("question").innerHTML = x + " &#215;" + y;
    correctAnswer = x*y;
    correctPosition = Math.floor((Math.random()*3)+1);
    document.getElementById("box"+correctPosition).innerHTML = correctAnswer;
    
    var answer = [correctAnswer];
    
    for(i=1; i<5; i++){
        if(i != correctPosition){
            var wrongAnswer;
            do{
                // create a wrong answer
                wrongAnswer = (1+ Math.round(9*Math.random()))*(1+ Math.round(9*Math.random()));
                
            }while(answer.indexOf(wrongAnswer) > -1)
            
            answer.push(wrongAnswer);
            
            document.getElementById("box"+i).innerHTML = wrongAnswer;
            
            
        }
    }
}

for(i=1;i<5;i++){
   document.getElementById("box"+i).onclick=function(){
        if(playing){
            if(this.innerHTML == correctAnswer){
                score++;
                document.getElementById("scorevalue").innerHTML = score;
                show("correct");
                qustionAndAnswer();
                setTimeout(function(){
                    hide("correct")
                },1000)
            }else{
                show("wrong");
                setTimeout(()=>{
                    hide("wrong")
                },1000)
            }
        }  
    } 
}



//if we click on the start/reset button
//if we are playing
    //Reload the page
//if we are not playing
    //set score to zero
    //show countdown box
    //reduce time by 1 sec loops
        //if timeleft
            //continue the game
            //change button Start to Reset
            //Genarate new qustion and answer
        //if no timeleft
            //Stop The game
            //show game over messege box

//if we click on answer box
    //if we are playing
        //if answer is correct
            //increase the score by one
            //show the correct box for one seconde
            //Genarate new qustion and answer
        //if answer is wrong
            //show the try again box for one seconde




/***************
*  Logic Two   *
****************
document.getElementById("startreset").innerHTML="Start Game";
var startreset = document.getElementById("startreset");
startreset.addEventListener("click",startgame);
var scorevalue = document.getElementById("scorevalue");
var timeremaining = document.getElementById("timeremaining");
var question = document.getElementById("question");
var box1 = document.getElementById("box1");
var box2 = document.getElementById("box2");
var box3 = document.getElementById("box3");
var box4 = document.getElementById("box4");
var resultbox;
var gameOver = document.getElementById("gameOver");
var result;
var score;

function startgame(){
    if(startreset.innerHTML == "Start Game"){
        scorevalue.innerHTML=0;
        timeremaining.style.display="block";
        var timeremainingvalue = document.getElementById("timeremainingvalue");
        startreset.innerHTML = "Reset Game";
        timeremaning();
        let num1 = Math.floor(Math.random()*10)+1;
        let num2 = Math.floor(Math.random()*10)+1;
        window.result = num1*num2;
        window.resultbox = document.getElementById("box"+Math.floor((Math.random()*3)+1));
        question.innerHTML = num1 + " &#215;" + num2;
        box1.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
        box2.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
        box3.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
        box4.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
        resultbox.innerHTML = num1*num2;
    }else{
        location.reload();
    }
    
}
function QA(){
    let num3 = Math.floor(Math.random()*10)+1;
    let num4 = Math.floor(Math.random()*10)+1;
    window.result = num3*num4;
    window.resultbox = document.getElementById("box"+Math.floor((Math.random()*3)+1));
    question.innerHTML = num3 + " &#215;" + num4;
    box1.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
    box2.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
    box3.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
    box4.innerHTML = (Math.floor(Math.random()*10)+1)*(Math.floor(Math.random()*10)+1);
    resultbox.innerHTML = num3*num4;
}
function timeremaning(){
    var i = 60;
    var myInterval = setInterval(() => {
        i--;
        timeremainingvalue.innerHTML= i;
        if(i<=0){
            clearInterval(myInterval);
            gameOver.style.display="block";
            gameOver.innerHTML = "Game over!<br>Your Score is"+score;
        }
    }, 1000);
}


var correct = document.getElementById("correct");
var wrong = document.getElementById("wrong");
function checkAnswer(boxno){
    if(boxno.innerHTML == result){
        window.score = scorevalue.innerHTML;
        score++;
        scorevalue.innerHTML=score;
        correct.style.display="block";
        QA();
        setTimeout(()=>{
            correct.style.display="none";
        },1000);
    }else{
        wrong.style.display="block";
        setTimeout(()=>{
            wrong.style.display="none";
        },1000);
    }
}
*/
