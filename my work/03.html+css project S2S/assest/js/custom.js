$(document).ready(function () {
    $('.done').owlCarousel({
        loop: true,
        margin: 10,
        navText: ["previous","next"],
        responsiveClass:true,
        responsive:{
            0:{
                items: 1,
                nav: true
            },
            600:{
                items: 3,
                nav: false

            },
            1000:{
                items: 1,
                nav: true,
                loop: true,
                autoplay: true,
                autoplayHoverPause: true
            }
        }
    });
    $('.old').owlCarousel({
        loop: true,
        margin: 10,
        items: 3,
        autoplay:true,
        autoplayHoverPause:true,
    }) 
});  