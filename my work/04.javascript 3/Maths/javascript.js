var playing = false;
var score;
var timeremening;
var myInterval;
var correctAnswer;
var correctPosition;

document.getElementById("startreset").addEventListener("click", startgame);

function startgame() {
    if (!playing) {
        playing = true;
        score = 0;
        document.getElementById("scorevalue").innerHTML = score;
        show("timeremaining");
        hide("gameOver");
        document.getElementById("startreset").innerHTML = "Reset Game";
        countdown();
        qustionAndAnswer();
    } else {
        location.reload();
    }
}

function hide(id) {
    document.getElementById(id).style.display = "none";
}

function show(id) {
    document.getElementById(id).style.display = "block";
}

function countdown() {
    timeremening = 60;
    myInterval = setInterval(function () {
        timeremening -= 1;
        document.getElementById("timeremainingvalue").innerHTML = timeremening;
        if (timeremening == 0) {
            playing = false;
            clearInterval(myInterval);
            show("gameOver");
            hide("timeremaining");
            document.getElementById("startreset").innerHTML = "Start Game";
            document.getElementById("gameOver").innerHTML = "<p>Game is Over!</P><p>Your score is" + score + "</p>";
        }
    }, 1000);
}

function qustionAndAnswer() {
    var x = 1 + Math.floor(Math.random() * 10);
    var y = 1 + Math.floor(Math.random() * 10);
    document.getElementById("question").innerHTML = x + " &#215;" + y;
    correctAnswer = x * y;
    correctPosition = Math.floor((Math.random() * 3) + 1);
    document.getElementById("box" + correctPosition).innerHTML = correctAnswer;

    var answer = [correctAnswer];

    for (i = 1; i < 5; i++) {
        if (i != correctPosition) {
            var wrongAnswer;
            do {
                wrongAnswer = (1 + Math.round(9 * Math.random())) * (1 + Math.round(9 * Math.random()));

            } while (answer.indexOf(wrongAnswer) > -1)

            answer.push(wrongAnswer);

            document.getElementById("box" + i).innerHTML = wrongAnswer;


        }
    }
}

for (i = 1; i < 5; i++) {
    document.getElementById("box" + i).onclick = function () {
        if (playing) {
            if (this.innerHTML == correctAnswer) {
                score++;
                document.getElementById("scorevalue").innerHTML = score;
                show("correct");
                qustionAndAnswer();
                setTimeout(function () {
                    hide("correct")
                }, 1000)
            } else {
                show("wrong");
                setTimeout(() => {
                    hide("wrong")
                }, 1000)
            }
        }
    }
}
