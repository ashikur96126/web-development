(function ($) {
	"use strict";

	/* Enabling support for new HTML5 tags for IE6, IE7 and IE8 */
	if (navigator.appName == 'Microsoft Internet Explorer') {
		if ((navigator.userAgent.indexOf('MSIE 6.0') >= 0) || (navigator.userAgent.indexOf('MSIE 7.0') >= 0) || (navigator.userAgent.indexOf('MSIE 8.0') >= 0)) {
			document.createElement('header')
			document.createElement('nav')
			document.createElement('section')
			document.createElement('aside')
			document.createElement('footer')
			document.createElement('article')
			document.createElement('hgroup')
			document.createElement('figure')
			document.createElement('figcaption')
			document.createElement('video')
		}
	}



})(jQuery);

jQuery(document).ready(function () {

	$('#owl_one').owlCarousel({
		smartSpeed: 450,
		loop: true,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		dots: true,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true,
				dots: false,
			},
			768: {
				items: 1,
				nav: true,
			},
			1000: {
				items: 1,
				nav: true,
				loop: true,
			}
		}
	});

}
);

