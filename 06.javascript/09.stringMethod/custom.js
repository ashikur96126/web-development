function b(){
    return document.write("<br>");
}

var sentence = "I am a boy & I am a Student also";
document.write(sentence);
b();


// string.length

var x = sentence.length;
document.write(x);
b();

// string.indexOf()

var indexNo = sentence.indexOf("am");

document.write(indexNo);
b();

// slice(strabt, end);
var sliceFruit = sentence.slice(13,27);
document.write(sliceFruit);
b();

//Or
var A = sentence.indexOf(" I");
var B = sentence.indexOf(" also");

var nextSentence = sentence.slice(A,B);
document.write(nextSentence);
b();


// replace(param1, param2);
var girl = sentence.replace("boy","girl");
document.write(girl);
b();

///toUpperCase();

var upperCase = sentence.toUpperCase();
document.write(upperCase);
b();

///toUpperCase();

var lowerCase = sentence.toLowerCase();
document.write(lowerCase);
b();

//charAt()

var charecterAt = sentence.charAt(2);
document.write(charecterAt);
b();


