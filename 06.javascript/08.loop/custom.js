document.write("<h1>For Loop</h1>");

for( i=1; i<=10; i++ ){
    document.write(i+" ,");
}

document.write("<h1>While Loop</h1>");
var a = 15;
while(a<=30){
    document.write("This is "+a+", ");
    a++
};


document.write("<h1>Do while Loop</h1>");
var x = 0;
do{
    
    document.write("<br>I want to Be a Developer");
    x++;
}while(x<10);