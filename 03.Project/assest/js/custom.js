$(document).ready(function(){
  $('.slider').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    navText: ["previous","next"],
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false

        },
        1000:{
            items:1,
            nav:true,
            loop:true,
            autoplay:true,
            autoplayHoverPause:true
        }
    }
  }) 
});  