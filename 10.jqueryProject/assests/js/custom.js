
var startButton = $("#btn2");
var userText = $("#userText");
var startTime;

//gretting
$("#btn1").click(function(){
    let name = $("#name").val();
    if(name.length>0){
       $("#username").text(name);
        $("#startTest").show(); $("#greeting,#randomSentence,#userText").hide();
    }else{
        alert("Please type your name first")
    }
});

startButton.click(function(){
    //jodi buttoner text start hoy
    if(startButton.text() == "Start"){
        //buttoner text start theke Done a porinoto hobe.
        startButton.text("Try Again");
        //userText and randomSentence show korbe
        $("#randomSentence, #userText").show();
        //greeting message hide hobe
        $("#startTest h3").hide();
        //Randomly akta sentence asbe
        $.ajax({
            url:"assests/file/sentence.json",
            dataType:"json",
            success: function(data){

                var randomNumber = Math.floor(Math.random()*data.length);

                $("#randomSentence").text(data[randomNumber]);
            }
        }); 
        //textarea cursor active korbo
        userText.focus();
        //start korar somoyti store korbo
        startTime = new Date().getTime();
    }
    //jodi buttoner text Start na hoy
    else{
        
        //textarear value guli store korbo
        var userSentence = userText.val();
        //randomly j sentence ti asbe seta store korbo
        var currentSentence = $("#randomSentence").text();
        //calculate correct words and store it
        var z = compareWords(userSentence,currentSentence);
        
        if(z[0]>=1){
            //buttoner text Done theke Start a porinoto hobe.
            startButton.text("Start");
            //textarear value null kore dibo
            userText.val(null);
            //textarea hide korbo
            userText.hide();
            //End korar somoyti store korbo
            var endTime = new Date().getTime();
            //randomSentence a reasult show korbe.
           //calculate userWorda and store it
            var userWords = userSentence.split(" ").length;
            //calculate total time that user spend
            var spendTimes = (endTime-startTime)/1000;
            //get speed per minute
            var speed = typingSpeed(userWords,spendTimes);
            
            $("#randomSentence").text( "You can type "+speed+" word per minute. and you are type "+z[0]+" correct word & "+z[1]+" wrong word.");
        }else{
            alert("Please type minimum one correct words");
        }
    };
});

function typingSpeed(userWords,spendTimes){
    var x = (userWords/spendTimes)*60;
    return Math.floor(x);
};

function compareWords(sentenceOne,sentenceTwo){
    var x = sentenceOne.split(" ");
    var y = sentenceTwo.split(" ");
    console.log(y);
    
    var corretwords = 0;
    var wrongwords = 0;
    
    y.forEach(function(item,index){
        
        if(item == x[index]){
            corretwords++;
        }else{
            wrongwords++;
        };
        
    });
    
    
    return [
        corretwords,
        wrongwords
    ]
};

   
    
    
